import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { AppComponent } from '../app.component';
import { SearcherComponent } from '../components/searcher/searcher.component';
import { FavouriteComponent } from '../components/favourite/favourite.component';
import { SearchService } from '../services/search-service.service';
import { FilterPipe } from '../pipes/filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    SearcherComponent,
    FavouriteComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,


  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
