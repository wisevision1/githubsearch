import { NgModule, Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Git } from './api.service';
import { pipe } from '@angular/core/src/render3/pipe';

class SearchItem {
  constructor(public name: string, public url: string) { }
}

@Injectable(
  //   {
  //   providedIn: 'root'
  // }
)
export class SearchService {
  apiRoot: string = 'https://api.github.com/search/repositories';
  queryUrl: string = '?q=';
  constructor(private http: Http) {
  }
  search(term: Observable<string>) {
    return term.pipe(debounceTime(1000))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntries(term)))
  }
  searchEntries(term) {
    return this.http
      .get(this.apiRoot + this.queryUrl + term)
      .pipe(map(res => res.json()));
  }
}
export class SearchServiceService {

  constructor() { }
}
