import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class Git {

  constructor(private httpClient: HttpClient) { }
  public getGit() {
    return this.httpClient.get(`https://api.github.com/search/repositories?q=KEYWORD`);
  }
}
