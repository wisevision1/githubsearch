import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Git } from '../../services/api.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { SearchService } from '../../services/search-service.service';
import { Subject } from 'rxjs/';
import { FilterPipe } from '../../pipes/filter.pipe';


@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.css'],
  providers: [Git, SearchService]
})
export class SearcherComponent implements OnInit {
  constructor(private router: Router, private git: Git, private searchService: SearchService) { }
  results: Object;
  searchTerm$ = new Subject<string>();
  uniqueArray;
  storageArray = [];
  onFilter(event) {
    this.uniqueArray = this.results;
    const filter = this.uniqueArray.filter(index => {
      return index.language === event;
    });
    this.uniqueArray = filter;
    document.getElementById('vasya').style.display = 'none';
    return this.uniqueArray;
  }

  addToStorage(event) {
    this.storageArray.push(event);
    alert('Git added to your favourites');
  }

  navFavourite() {
    localStorage.setItem('storeObj', JSON.stringify(this.storageArray));
    this.router.navigateByUrl('/favourite');
  }

  ngOnInit() {
    localStorage.removeItem('storeObj');
    this.searchService.search(this.searchTerm$)
      .subscribe(results => {
        this.results = results.items;
      });
  }
}
