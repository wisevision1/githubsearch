import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  constructor() { }
  results;
  ngOnInit() {
    const getObject = JSON.parse(localStorage.getItem('storeObj'));
    this.results = getObject;
  }

}
